import App from './App.svelte';

const app = new App({
	target: document.body,
	props: {
		name: 'nico'
	}
});

export default app;